#! /usr/bin/env python3
import os
import requests
import time

flask_run_port = os.getenv("FLASK_RUN_PORT")
consul_register_endpoint = "http://localhost:8500/v1/agent/service/register"

template = {
    "name": "api",
    "tags": ["flask"],
    "address": "",
    "port": int(flask_run_port),
    "checks": [
        {"http": "http://localhost:{}/hostname".format(flask_run_port), "interval": "5s"}
    ]
}

for retry in range(10):
    res = requests.put(consul_register_endpoint, json=template)
    print("Attempt num:", retry, "Response Status:", res.status_code)
    if res.status_code == 200:
        print("Registering successful!")
        break
    else:
        print(res.text)
    time.sleep(1)
else:
    print("Run out of retires. So something went wrong.")
