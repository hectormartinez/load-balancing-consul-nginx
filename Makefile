.PHONY: build
build:
	(cd base-image && docker build . -t base-image:latest)
	(cd api && docker build . -t api:latest)
	(cd load-balancer && docker build . -t load-balancer:latest)
